use std::borrow::Cow;
use std::convert::TryFrom;
use std::io::{self, Read};
use thiserror::Error;

macro_rules! le {
    ($typ: ident, $slice: expr, $offset: expr) => {
        $typ::from_le_bytes(
            std::convert::TryFrom::try_from(
                &$slice[$offset..$offset + std::mem::size_of::<$typ>()],
            )
            .unwrap(),
        )
    };
}

#[derive(Error, Debug)]
pub enum Error {
    #[error("io error")]
    IoError(#[from] io::Error),
    #[error("parse error")]
    ParseError,
    #[error("wrong magic octets")]
    MagicError,
}

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Clone, Copy, Debug)]
pub struct HeaderType {
    /// continued page
    pub cont: bool,
    /// begin of stream
    pub bos: bool,
    /// end of stream
    pub eos: bool,
}

impl HeaderType {
    pub fn from_u8(val: u8) -> HeaderType {
        HeaderType {
            cont: val & 0x01 == 0x01,
            bos: val & 0x02 == 0x02,
            eos: val & 0x04 == 0x04,
        }
    }
}

#[derive(Clone, Debug)]
pub struct Page {
    contents: Vec<u8>,
}

impl Page {
    pub fn from_read<R: Read>(read: &mut R) -> Result<Self> {
        let mut contents = vec![0u8; 27];
        read.read_exact(&mut contents[0..4])?;
        if &contents[0..4] != b"OggS" {
            Err(Error::MagicError)
        } else {
            read.read_exact(&mut contents[4..27])?;
            let num_segments = usize::from(le!(u8, &contents, 26));
            contents.resize(27 + num_segments, 0u8);
            let segment_table = &mut contents[27..27 + num_segments];
            read.read_exact(segment_table)?;
            let payload_size = segment_table
                .iter()
                .map(|len| usize::from(*len))
                .sum::<usize>();
            contents.resize(27 + num_segments + payload_size, 0u8);
            read.read_exact(&mut contents[27 + num_segments..27 + num_segments + payload_size])?;
            Ok(Page { contents })
        }
    }

    pub fn as_bytes(&self) -> &[u8] {
        &self.contents
    }

    pub fn magic(&self) -> &[u8] {
        &self.contents[0..4]
    }

    pub fn version(&self) -> u8 {
        le!(u8, self.contents, 4)
    }

    pub fn header_type(&self) -> HeaderType {
        HeaderType::from_u8(le!(u8, self.contents, 5))
    }

    pub fn granule_position(&self) -> u64 {
        le!(u64, self.contents, 6)
    }

    pub fn bitstream_serial_number(&self) -> u32 {
        le!(u32, self.contents, 14)
    }

    pub fn page_sequence_number(&self) -> u32 {
        le!(u32, self.contents, 18)
    }

    pub fn crc_checksum(&self) -> u32 {
        le!(u32, self.contents, 22)
    }

    pub fn num_segments(&self) -> u8 {
        le!(u8, self.contents, 26)
    }

    pub fn segment_table(&self) -> &[u8] {
        // TODO: ensure that this is well-formed, i.e. the last entry must be < 255
        &self.contents[27..self.header_size()]
    }

    pub fn segments<'a>(&'a self) -> impl Iterator<Item = &'a [u8]> {
        let offset = self.header_size();
        self.segment_table()
            .iter()
            .scan(offset, move |offset, len| {
                let len = usize::from(*len);
                let segment = &self.contents[*offset..*offset + len];
                *offset += len;
                Some(segment)
            })
    }

    pub fn num_packets(&self) -> usize {
        self.segment_table()
            .iter()
            .filter(|len| **len < 255)
            .count()
    }

    pub fn packets<'a>(&'a self) -> impl Iterator<Item = &'a [u8]> {
        let offset = self.header_size();
        self.segment_table()
            .iter()
            .scan((offset, offset), move |span, len| {
                if *len == 255 {
                    span.1 += usize::from(*len);
                    Some(None)
                } else {
                    span.1 += usize::from(*len);
                    let segment = &self.contents[span.0..span.1];
                    *span = (span.1, span.1);
                    Some(Some(segment))
                }
            })
            .flatten()
    }

    pub fn show(&self) -> String {
        format!(
            r#"
Page {{
    magic:                    {:x?}
    version:                  {}
    header_type:              {:?}
    granule_position:         {}
    bitstream_serial_number:  {}
    page_sequence_number:     {}
    crc_checksum:             {:x}
    num_segments:             {}
    segment_table:            {:?}
}}
"#,
            self.magic(),
            self.version(),
            self.header_type(),
            self.granule_position(),
            self.bitstream_serial_number(),
            self.page_sequence_number(),
            self.crc_checksum(),
            self.num_segments(),
            self.segment_table(),
        )
    }

    /// Header size
    pub fn header_size(&self) -> usize {
        27 + usize::from(self.num_segments())
    }

    /// Page size
    pub fn page_size(&self) -> usize {
        self.contents.len()
    }

    /// Compute the CRC32 checksum
    pub fn compute_crc32(&self) -> u32 {
        // NOTE: we actually need
        //     `let mut hasher = crc_any::CRCu32::create_crc(0x04c11db7, 32, 0x0, 0x0, false);`
        // but crc32posix() has almost the same parameters (except the final xor mask), while also
        // providing a predefined crc table
        let mut hasher = crc_any::CRCu32::crc32posix();
        hasher.digest(&self.contents[0..22]);
        hasher.digest(&[0u8; 4]); // skip crc field
        hasher.digest(&self.contents[26..]);
        use std::ops::BitXor;
        hasher.get_crc().bitxor(0xffffffff)
    }

    pub fn parse_opus_head(&self) -> Result<OpusHead> {
        // TODO: check assertion during parsing
        assert_eq!(self.num_packets(), 1);

        let opus_head = OpusHead {
            contents: self.packets().nth(0).unwrap(),
        };
        // TODO: more extensive validation
        if opus_head.magic() != b"OpusHead" {
            Err(Error::MagicError)
        } else {
            Ok(opus_head)
        }
    }

    pub fn parse_opus_tags(&self) -> Result<OpusTags> {
        let opus_tags = OpusTags {
            // TODO: error handling
            contents: Cow::Borrowed(self.packets().nth(0).unwrap()),
        };
        // TODO: more extensive validation
        if opus_tags.magic() != b"OpusTags" {
            Err(Error::MagicError)
        } else {
            Ok(opus_tags)
        }
    }

    pub fn with_opus_tags(&self, opus_tags: &OpusTags) -> Result<Self> {
        let num_segments = opus_tags.contents.len() / 255 + 1;
        let last_segment_size = opus_tags.contents.len() % 255;

        let mut contents = vec![0u8; 27 + num_segments + opus_tags.contents.len()];
        for i in 0..22 {
            contents[i] = self.contents[i]
        }
        // skip crc
        contents[26] = u8::try_from(num_segments).unwrap();
        for i in 27..27 + num_segments - 1 {
            contents[i] = 0xff;
        }
        contents[27 + num_segments - 1] = u8::try_from(last_segment_size).expect("within u8 range");
        let read = &mut opus_tags.contents.as_ref();
        read.read_exact(&mut contents[27 + num_segments..])?;

        let mut page = Page { contents };
        let crc = page.compute_crc32().to_le_bytes();
        let read = &mut &crc[..];
        read.read_exact(&mut page.contents[22..26])?;

        Ok(page)
    }
}

#[derive(Clone, Debug)]
pub struct OpusHead<'a> {
    contents: &'a [u8],
}

impl<'a> OpusHead<'a> {
    pub fn as_bytes(&self) -> &[u8] {
        &self.contents
    }

    pub fn magic(&self) -> &[u8] {
        &self.contents[0..8]
    }

    pub fn version(&self) -> u8 {
        le!(u8, self.contents, 8)
    }

    pub fn channel_count(&self) -> u8 {
        le!(u8, self.contents, 9)
    }

    pub fn pre_skip(&self) -> u16 {
        le!(u16, self.contents, 10)
    }

    pub fn input_sample_rate(&self) -> u32 {
        le!(u32, self.contents, 12)
    }

    pub fn output_gain(&self) -> u16 {
        le!(u16, self.contents, 16)
    }

    pub fn mapping_family(&self) -> u8 {
        le!(u8, self.contents, 18)
    }

    pub fn channel_mapping_table(&self) -> &[u8] {
        &self.contents[19..]
    }

    pub fn show(&self) -> String {
        format!(
            r#"
OpusHead {{
    magic:                  {:x?}
    version:                {}
    channel_count:          {}
    pre_skip:               {}
    input_sample_rate:      {}
    output_gain:            {}
    mapping_family:         {}
    channel_mapping_table:  {:x?}
}}
"#,
            self.magic(),
            self.version(),
            self.channel_count(),
            self.pre_skip(),
            self.input_sample_rate(),
            self.output_gain(),
            self.mapping_family(),
            self.channel_mapping_table(),
        )
    }
}

#[derive(Clone, Debug)]
pub struct OpusTags<'a> {
    contents: Cow<'a, [u8]>,
}

impl<'a> OpusTags<'a> {
    pub fn as_bytes(&self) -> &[u8] {
        &self.contents
    }

    pub fn magic(&self) -> &[u8] {
        &self.contents[0..8]
    }

    pub fn vendor_string_len(&self) -> u32 {
        le!(u32, self.contents, 8)
    }

    pub fn vendor_string(&self) -> Cow<str> {
        let vs_len = usize::try_from(self.vendor_string_len()).unwrap();
        String::from_utf8_lossy(&self.contents[12..12 + vs_len])
    }

    pub fn comment_list_len(&self) -> u32 {
        let vs_len = usize::try_from(self.vendor_string_len()).unwrap();
        le!(u32, self.contents, 12 + vs_len)
    }

    pub fn comment_list(&'a self) -> impl Iterator<Item = Cow<'a, str>> {
        let vs_len = usize::try_from(self.vendor_string_len()).unwrap();
        let cl_len = usize::try_from(self.comment_list_len()).unwrap();
        let offset = 16 + vs_len;
        (0..cl_len).scan(offset, move |offset, _| {
            let len = usize::try_from(le!(u32, self.contents, *offset)).unwrap();
            *offset += 4;
            let comment = String::from_utf8_lossy(&self.contents[*offset..*offset + len]);
            *offset += len;
            Some(comment)
        })
    }

    pub fn show(&self) -> String {
        format!(
            r#"
OpusTags {{
    magic:                 {:x?}
    vendor_string_len:     {}
    vendor_string:         {}
    comment_list_len:      {}
    comments:              {:?}
}}
"#,
            self.magic(),
            self.vendor_string_len(),
            self.vendor_string(),
            self.comment_list_len(),
            self.comment_list().collect::<Vec<_>>(),
        )
    }

    pub fn set_comment_list(&mut self, comment_list: &[String]) {
        let pad_size = self.contents.len();

        let unchanged = 12 + usize::try_from(self.vendor_string_len()).unwrap();
        self.contents.to_mut().resize(unchanged, 0u8);

        let comment_list_len = u32::try_from(comment_list.len()).unwrap();
        self.contents
            .to_mut()
            .extend(comment_list_len.to_le_bytes().iter().copied());

        for comment in comment_list.iter() {
            let comment_len = u32::try_from(comment.as_bytes().len()).unwrap();
            self.contents
                .to_mut()
                .extend(comment_len.to_le_bytes().iter().copied());
            self.contents.to_mut().extend(comment.as_bytes());
        }

        if self.contents.len() > 256 * 254 {
            // TODO: handle error
            panic!(
                "OpusTags of length {} > {} not supported",
                self.contents.len(),
                256 * 254
            );
        } else if self.contents.len() < pad_size {
            self.contents.to_mut().resize(pad_size, 0u8);
        }
    }

    pub fn grow(&mut self) {
        let new_len = (256 * 254).min(255 + 4 * self.contents.len() / 3);
        self.contents.to_mut().resize(new_len, 0u8);
    }

    pub fn shrink(&mut self) {
        let new_len = 8
            + 4
            + usize::try_from(self.vendor_string_len()).unwrap()
            + 4
            + self
                .comment_list()
                .map(|s| 4 + s.as_bytes().len())
                .sum::<usize>();
        self.contents.to_mut().resize(new_len, 0u8);
    }
}
