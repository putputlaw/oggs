use clap::Clap;
use oggs::*;
use std::convert::TryInto;
use std::io::{Read, Seek, SeekFrom, Write};
use std::path::PathBuf;

#[derive(Clap)]
#[clap(author = "Long Huynh Huu")]
pub struct Args {
    #[clap(subcommand)]
    cmd: Command,
}

#[derive(Clap)]
pub enum Command {
    /// Verify an ogg/opus file
    #[clap(name = "validate")]
    Validate {
        /// opus file
        #[clap(parse(from_os_str))]
        filepath: PathBuf,
    },
    /// Show tags
    #[clap(name = "get")]
    Get {
        /// opus file
        #[clap(parse(from_os_str))]
        filepath: PathBuf,
        /// select a tag to show (case insensitive)
        #[clap(short = "t", long = "tag")]
        tag: Option<String>,
    },
    /// Set a tag
    #[clap(name = "set")]
    Set {
        /// opus file
        #[clap(parse(from_os_str))]
        filepath: PathBuf,
        /// select a tag (case insensitive)
        #[clap(short = "t", long = "tag")]
        tag: String,
        /// the new value
        #[clap(short = "v", long = "val")]
        val: String,
    },
    /// Show a page
    #[clap(name = "page")]
    Page {
        /// opus file
        #[clap(parse(from_os_str))]
        filepath: PathBuf,
        /// page index
        #[clap(short = "i", long = "index", default_value = "0")]
        index: usize,
    },
    /// Show the header
    #[clap(name = "header")]
    Header {
        /// opus file
        #[clap(parse(from_os_str))]
        filepath: PathBuf,
    },
    /// Shrink the comments page
    #[clap(name = "shrink")]
    Shrink {
        /// opus file
        #[clap(parse(from_os_str))]
        filepath: PathBuf,
    },
}

fn main() -> Result<()> {
    let args = Args::parse();
    match args.cmd {
        Command::Validate { filepath } => {
            let mut opus = std::fs::File::open(filepath)?;
            loop {
                match Page::from_read(&mut opus) {
                    Ok(page) => {
                        if page.crc_checksum() != page.compute_crc32() {
                            eprintln!("Page {} contains an error", page.page_sequence_number());
                            std::process::exit(1);
                        }
                    }
                    // TODO: check whether eof or other error happened
                    Err(_) => break,
                }
            }
        }

        Command::Get { filepath, tag } => {
            let mut opus = std::fs::File::open(filepath)?;
            let _ = Page::from_read(&mut opus)?;
            let page = Page::from_read(&mut opus)?;
            if let Some(tag) = tag {
                let tag = tag.to_lowercase();
                for c in page.parse_opus_tags()?.comment_list() {
                    if let Some((key, value)) = parse_tag(c.as_ref()) {
                        if tag == key.to_lowercase() {
                            println!("{}", value);
                            std::process::exit(0);
                        }
                    }
                }
                eprintln!("Tag `{}` not found", tag);
                std::process::exit(1);
            } else {
                for comment in page.parse_opus_tags()?.comment_list() {
                    println!("{}", comment);
                }
            }
        }

        Command::Set { filepath, tag, val } => {
            let mut opus = std::fs::File::open(&filepath)?;
            let page0 = Page::from_read(&mut opus)?;
            let page = Page::from_read(&mut opus)?;
            drop(opus);

            let mut opus_tags = page.parse_opus_tags()?;

            // replace tag, if it exists
            let mut new_comment_list: Vec<String> =
                opus_tags.comment_list().map(|s| s.to_string()).collect();
            let tag_lower = tag.to_lowercase();
            let mut tag_is_set = false;
            for i in 0..new_comment_list.len() {
                if let Some((key, _)) = parse_tag(&new_comment_list[i]) {
                    if key.to_lowercase() == tag_lower {
                        new_comment_list[i] = format!("{}={}", tag, val);
                        tag_is_set = true;
                        break;
                    }
                }
            }
            // if unset, add new tag
            if !tag_is_set {
                new_comment_list.push(format!("{}={}", tag, val));
            }

            let opus_tags_old_len = opus_tags.as_bytes().len();
            opus_tags.set_comment_list(&new_comment_list[..]);
            let opus_tags_new_len = opus_tags.as_bytes().len();

            if opus_tags_new_len > opus_tags_old_len {
                opus_tags.grow();
                let new_page = page.with_opus_tags(&opus_tags)?;
                assert_eq!(new_page.compute_crc32(), new_page.crc_checksum());

                // we have to shift the whole file
                let mut buf: Vec<u8> = Vec::new();
                buf.write(&page0.as_bytes())?;
                buf.write(&new_page.as_bytes())?;
                {
                    // read the rest
                    let mut opus = std::fs::File::open(&filepath)?;
                    let _ = Page::from_read(&mut opus)?;
                    let _ = Page::from_read(&mut opus)?;
                    opus.read_to_end(&mut buf)?;
                }
                std::fs::write(&filepath, &buf)?;
            } else {
                let new_page = page.with_opus_tags(&opus_tags)?;
                assert_eq!(new_page.compute_crc32(), new_page.crc_checksum());

                {
                    // no need to shift
                    let mut opus = std::fs::OpenOptions::new()
                        .write(true)
                        .truncate(false)
                        .open(&filepath)?;

                    opus.seek(SeekFrom::Start(page0.as_bytes().len().try_into().unwrap()))?;
                    opus.write(&new_page.as_bytes())?;
                }
            }
        }

        Command::Page { filepath, index } => {
            let mut opus = std::fs::File::open(filepath)?;
            let mut page = Page::from_read(&mut opus)?;
            for _ in 0..index {
                page = Page::from_read(&mut opus)?;
            }
            println!("{}", page.show());
        }

        Command::Header { filepath } => {
            let mut opus = std::fs::File::open(filepath)?;
            let page = Page::from_read(&mut opus)?;
            println!("{}", page.parse_opus_head()?.show());
        }

        Command::Shrink { filepath } => {
            let mut opus = std::fs::File::open(&filepath)?;
            let page0 = Page::from_read(&mut opus)?;
            let page = Page::from_read(&mut opus)?;
            let mut opus_tags = page.parse_opus_tags()?;
            opus_tags.shrink();
            let new_page = page.with_opus_tags(&opus_tags)?;

            let mut buf = Vec::new();
            buf.write(page0.as_bytes())?;
            buf.write(new_page.as_bytes())?;
            opus.read_to_end(&mut buf)?;

            std::fs::write(&filepath, buf)?;
        }
    }
    Ok(())
}

fn parse_tag(comment: &str) -> Option<(&str, &str)> {
    comment
        .find('=')
        .and_then(|i| Some((comment.get(0..i)?, comment.get(i + 1..)?)))
}
