Read and edit Ogg/Opus Tags
===========================

**Warning** This ain't production ready, expect silly things to happen.

## Resources

- Ogg Spec: https://xiph.org/ogg/doc/rfc3533.txt
- Further info: https://xiph.org/ogg/doc/index.html
- Opus docs: https://opus-codec.org/docs/
- Ogg Encapsulation for the Opus Audio Codec: https://tools.ietf.org/html/rfc7845.html
- Ross N. William's article on CRC (archived version): http://archive.is/gw9RC

## License

Licensed under either of

- Apache License, Version 2.0, (LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0)
- MIT license (LICENSE-MIT or http://opensource.org/licenses/MIT)

at your option.

## TODO

- [ ] support vorbis
- [ ] write test cases
